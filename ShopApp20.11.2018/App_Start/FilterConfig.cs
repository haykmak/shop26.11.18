﻿using System.Web;
using System.Web.Mvc;

namespace ShopApp20._11._2018
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
