﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Shop_Repository;

namespace ShopApp20._11._2018.Controllers
{
    public class ProductController : Controller
    {
        ShopRepository repository = new ShopRepository();

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Product product)
        {
            FixErrorsforCreate(product);

            if (ModelState.IsValid)
            {
                repository.AddProduct(product);
                ViewBag.Message = "Ավելացվեց նոր ապրանքատեսակ";
            }
            return View();
        }


        public ActionResult Edit(string code)
        {
            var product = repository.GetProducts().First(p => p.Code == code);
            return View(product);
        }

        [HttpPost]
        public ActionResult Edit(Product product) //TODO
        {
            //var products = repository.GetProducts();

            //if (product.Name != null && products.Any(p => p.Name.ToLower() == product.Name.ToLower() &&
            //       p.Color?.ToLower() == product.Color?.ToLower() && (p.Code != product.Code)))
            //{
            //    ModelState.AddModelError("", "Անվանման և գույնի համընկնում");
            //}
            if (ModelState.IsValid)
            {
                repository.Update(product);
                return RedirectToAction("GetProducts");
            }
            return View(product);
        }



        public ActionResult Delete(string code)
        {
            if (repository.GetInventories().Any(i => i.Product_Code.ToLower() == code.ToLower()))
            {
                ModelState.AddModelError("", $"Պահեստում առկա են {code} ապրանքատեսակի նմուշներ");
            }

            if (ModelState.IsValid)
            {
                repository.RemoveProduct(code);
            }
            return View("GetProducts", repository.GetProducts());
        }

        public ActionResult GetProducts()
        {
            var products = repository.GetProducts();
            return View(products);
        }

        private void FixErrorsforCreate(Product product)
        {

            var products = repository.GetProducts();

            if (products.Select(p => p.Code.ToLower()).Contains(product.Code.ToLower()))
            {
                ModelState.AddModelError("", $"{product.Code} կոդով ապրանք գոյություն ունի");
            }
            else
            {
                if (product.Name != null && products.Any(p => p.Name.ToLower() == product.Name.ToLower() &&
                (p.Color?.ToLower()) == (product.Color?.ToLower())))
                {
                    ModelState.AddModelError("", "Անվանման և գույնի համընկնում");
                }
            }
        }




        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }


    }
}