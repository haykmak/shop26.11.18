﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Shop_Repository;
using PagedList;

namespace ShopApp20_11_2018.Controllers
{
    public class HomeController : Controller
    {
        ShopRepository repository = new ShopRepository();
        
        public ActionResult Index(int? page)
        {
            var inv = repository.GetInventories();
            int pageSize = 5;
            int pageNumber = (page ?? 1);
            return View(inv.ToPagedList(pageNumber, pageSize));
        }

        public ActionResult Trade()
        {
            ViewBag.Codes = repository.GetProducts().Select(p => p.Code);
            ViewBag.Names = repository.GetProducts().Select(p => p.Name).ToHashSet();
            ViewBag.Colors = repository.GetProducts().Select(p => p.Color).ToHashSet();
            return View();
        }


        [HttpPost]
        public ActionResult Purchase(Inventory inventory)
        {
            repository.Purchase(inventory.Product_Code, inventory.Size, inventory.Quantity);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Sell(Inventory inventory)
        {
            var inventories = repository.GetInventories();
            ViewBag.Codes = repository.GetProducts().Select(p => p.Code);
            ViewBag.Names = repository.GetProducts().Select(p => p.Name).ToHashSet();
            ViewBag.Colors = repository.GetProducts().Select(p => p.Color).ToHashSet();

            if (!inventories.Any(i => i.Product_Code == inventory.Product_Code && i.Size == (inventory.Size == null ? "" : inventory.Size.ToUpper())))
            {
                ModelState.AddModelError("", "Պահեստում չկա նման ապրանք");
                
                return View("Trade");
            }
            else
            {
                var inv = inventories.First(i => i.Product_Code == inventory.Product_Code && i.Size == (inventory.Size == null ? "" : inventory.Size.ToUpper()));
                if (inv.Quantity < inventory.Quantity)
                {
                    ModelState.AddModelError("", $"Անբավարար քանակություն: Պահեստում առկա է {inv.Quantity} հատ");
                    return View("Trade");
                }
                else
                {
                    repository.Sell(inventory.Product_Code, inventory.Size, inventory.Quantity);
                    return RedirectToAction("Index");
                }
            }
        }


        //public ActionResult Delete(string code, string size)
        //{
        //    var inv = repository.GetInventories().First(i => i.Product_Code == code && i.Size == (size == null ? "" : size.ToUpper()));
        //    return View(inv);
        //}

     
       
        public ActionResult Delete(string code, string size)
        {
            repository.RemoveInventory(code, size);
            return RedirectToAction("Index");
        }

        public ActionResult GetDetails(string code)
        {
            var product = repository.GetProducts().First(p => p.Code == code);

            return PartialView(product);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }
}